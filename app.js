const express = require("express");
const morgan = require("morgan");
const createError = require("http-errors");
require("dotenv").config();
require("./Helpers/initMonogodb");

const AuthRoute = require("./Routes/AuthRoute");
const app = express();
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // the body can accept both the json and file format

app.get("/", async (req, res, next) => {
  res.send("Hello from Express!");
});

app.use("/auth", AuthRoute);

// catch handler to show error if the route not exist
app.use(async (req, res, next) => {
  next(createError.NotFound("This route not exist !"));
});

// the next(error) param will triger this function
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
  console.log(`Server is running on Port ${PORT}`);
});
