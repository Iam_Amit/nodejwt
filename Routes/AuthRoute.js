const express = require("express");
const router = express.Router();
const createError = require("http-errors");
const User = require("../Models/User_model");
const { authSchema } = require("../Helpers/validation_schema");

router.post("/register", async (req, res, next) => {
  try {
    // const { email, password } = req.body;
    // if (!email || !password) throw createError.BadRequest();

    const result = await authSchema.validateAsync(req.body);

    const doesExist = await User.findOne({ email: result.email });
    if (doesExist)
      throw createError.Conflict(`${result.email} is already been registerd !`);

    const user = new User(result);
    const saveUser = await user.save();
    res.send(saveUser);
  } catch (error) {
    if (error.isJoi === true) error.status = 422; // 422 unprocessable entity
    next(error);
  }
});

router.post("/login", async (req, res, next) => {
  res.send("Login route");
});

router.post("/refresh-token", async (req, res, next) => {
  res.send("Refresh Token route");
});

router.delete("/logout", async (req, res, next) => {
  res.send("Logout route");
});
module.exports = router;
